import readline from "readline-sync"

let angka :string|number = readline.question("Masukkan angka : ")
angka = Number(angka)
let str = angka.toString()
let start = true
let totalAkhir = 0
let indexAwal = 0
let stepLoop = 1
let afterIndex = 1
let reverseIndex = 0

let tampung: any[] = []

while (start){
    let front = str[indexAwal]
    let fromBehind = str[indexAwal]
    for(let i=afterIndex;i<str.length+afterIndex;i++){
        if(i%str.length !== indexAwal){
            front += str[i%str.length]
        }
    }
    for(let j=str.length-afterIndex;j>reverseIndex-1;j--){
        if(fromBehind.length === str.length){
            break
        }
        if(j !== indexAwal && j+str.length !== indexAwal){
            if(j < 0){
                if(Math.abs(j)%str.length === 0){
                    fromBehind += str[j%str.length]
                }else{
                    fromBehind += str[j%str.length+str.length]
                }
            }
            else{
                fromBehind += str[j%str.length]
            }
        }
    }
    if(!tampung.includes(front) && front.length === str.length){
        if(angka < parseInt(front)){
            console.log(parseInt(front))
            totalAkhir += 1
        }
        tampung.push(front)
        front = ""
    }
    if(!tampung.includes(fromBehind) && fromBehind.length === str.length){
        if(angka < parseInt(fromBehind)){
            console.log(parseInt(fromBehind))
            totalAkhir += 1
        }
        tampung.push(fromBehind)
        fromBehind = ""
    }
    stepLoop += 1 
    afterIndex += 1
    reverseIndex -= 1
    if(stepLoop === str.length){
        indexAwal += 1
        stepLoop = 0
        reverseIndex = 0
        afterIndex = indexAwal + 1
        reverseIndex = reverseIndex - indexAwal
    }
    if(indexAwal === str.length){
        start = false
    }
}

console.log(totalAkhir)
console.log(tampung)

